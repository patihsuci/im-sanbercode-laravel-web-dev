<?php

require_once('animal.php');
require_once('Frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name ."<br>"; // "shaun"
echo "Legs : " . $sheep->legs ."<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded ."<br> <br>"; // "no"



$Frog = new Frog("Buduk");
echo "Name : " . $Frog->name ."<br>"; // "Buduk"
echo "Legs : " . $Frog->legs ."<br>"; // 4
echo "cold blooded : " . $Frog->cold_blooded ."<br>"; // "no"
echo $Frog->lompat();
echo "<br>";

$ape = new ape("Kera Sakti");
echo "Name : " . $ape->name ."<br>"; // "Kera Sakti"
echo "Legs : " . $ape->legs ."<br>"; // 2
echo "Cold blooded : " . $ape->cold_blooded ."<br>"; // "no"
echo $ape->yell();
echo "<br>";

?>