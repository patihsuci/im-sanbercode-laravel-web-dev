<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PostController extends Controller
{
    public function create()
    {
        return view('Cast.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')->insert([
            'name' => $request['name'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
            
        ]);
        return redirect('/cast');
     
    }

    public function index()
    {
        
            $cast = DB::table('cast')->get();
     
            return view('Cast.tampil', ['cast' => $cast]);
        
            
    } 
    public function show($id){
         $cast = DB::table('cast')->find($id);

         return view('Cast.detail',['cast'=> $cast]);
    }

}
