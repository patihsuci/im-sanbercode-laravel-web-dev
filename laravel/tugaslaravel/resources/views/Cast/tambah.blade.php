@extends('layouts.master')
@section('title')
Halaman Cast
@endsection
@section('title')
Cast
@endsection
@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Cast Nama</label>
      <input type="text" name= name class="form-control">
    @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Cast Umur</label>
      <input type="integer" name= umur class="form-control">
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Cast Bio</label>
        <textarea name="bio" name= bio class="form-control" id="" cols="30" rows="10"></textarea>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  @endsection
  