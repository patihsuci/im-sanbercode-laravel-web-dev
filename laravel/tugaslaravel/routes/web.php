<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuntController;
use App\Http\Controllers\PostController;


Route::get('/', [HomeController::class,'dashboard'] );

Route::get('/pendaftaran', [AuntController::class,'daftar'] );

Route::post('/kirim', [AuntController::class,'kirim'] );

Route::get('/data-table', function(){
    return view('page.data-table');
});

Route::get('/table', function(){
    return view('page.table');
});


//CRUD
//Create Data
//Route mengarah ke form Post/Cast
Route::get('/cast/create',[PostController::class, 'create']);
//Route untuk menyimpan inputan kedalam database table cast
Route::post('/cast',[PostController::class, 'store']);

//Read data
Route::get('/cast', [PostController::class, 'index']);
//Road Detailkategory berdasarkan id
Route::get('/cast{id}', [PostController::class, 'show']);